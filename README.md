# SpotyApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.



## Primer endPoint utilizado
# con este endpoinr podemos obtener una lñista de todos los paises y realizar diferentes filtros para los mismos
https://restcountries.com/


## Consultamos Todos los paises de habla hispana
https://restcountries.com/v3.1/lang/spa


## Endpoints de navegación principal
# DOCS 


# BOWSE

METHOD
    GET	
ENDPOINT:
    /v1/recommendations/available-genre-seeds
USAGE:
    Get Available Genre Seeds
METHOD:
    GET	
ENDPOINT:
    /v1/browse/categories
USAGE:
    Get Several Browse Categories
METHOD:
    GET	
ENDPOINT:
    /v1/browse/categories/{category_id}
USAGE:
    Get Single Browse Category
METHOD:
    GET	
ENDPOINT:
    /v1/browse/categories/{category_id}/playlists
USAGE:
    Get Category's Playlists
METHOD:
    GET	
ENDPOINT:
    /v1/browse/featured-playlists
USAGE:
    Get Featured Playlists
METHOD:
    GET	
ENDPOINT:
    /v1/browse/new-releases
USAGE:
    Get New Releases
METHOD:
    GET	
ENDPOINT:
    /v:1/recommendations
USAGE:
    Get Recommendations
