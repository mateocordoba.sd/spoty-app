import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'

const url = 'https://api.spotify.com/v1/';

const headers = new HttpHeaders({
  'Authorization':'Bearer BQC1zdAsj_XSjcXatz6zgPYdMuQnkJPEdpXIBY-TMmLK5woIbbxTKyhtiHlOrkgUqsXbx3WmQOL4Ytj7QfI',
})




@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http:HttpClient) {
   
  }
  
  getAllGenres(){
//
    //})
    //this.http.get('https://api.spotify.com/v1/recommendations/available-genre-seeds',{headers})
    //  .subscribe(genres=>{
    //    console.log(genres);
    //    
    //  })
  }

  getQuery(query:string){
    return this.http.get(`${url}${query}`, {headers})
  }

  getAllRecomended(){
    
    return this.getQuery(`browse/new-releases`)
                    .pipe( map( (data:any ) => data['albums'].items));
  }

  

  searchArtist(termino:string){
    
    return this.getQuery(`search?q=${termino}&type=artist`)
                    .pipe( map( (data:any) => data['artists'].items));
  }

  getArtist(artistId:string){
    return this.getQuery(`artists/${artistId}`)

  }

  getTopTracks(artistId:string){
    return this.getQuery(`artists/${artistId}/top-tracks?country=us`)
                .pipe(map((data:any) => data['tracks']))

  }

}
