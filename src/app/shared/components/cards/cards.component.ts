import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  @Input() items:any[] = [];

  constructor(private spotify:SpotifyService, private router:Router) { }

  ngOnInit(): void {
  }

  seeArtist(item:any){
    let artistId:string = item.type === 'artist' ? item.id : item.artists[0].id;
    this.spotify.getArtist(artistId)
    this.router.navigate(['/artist',artistId])

  }


}
