import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-arista',
  templateUrl: './arista.component.html',
  styleUrls: ['./arista.component.scss']
})
export class AristaComponent implements OnInit {

  //artistId:string ="";
  artist:any = {};
  topTracks:any[]=[];
  loading:boolean =true;

  constructor(private router:Router, private activatedRoute:ActivatedRoute, private spotify:SpotifyService) {


    this.activatedRoute.params.subscribe(params =>{
      this.getArtist(params['id']);
      this.getTopTracks(params['id']);
      

  
    });
  }

  getArtist(id:string){
    this.spotify.getArtist(id).subscribe(artist =>{
      this.artist = artist;
      this.loading =false;

    });
  }

  getTopTracks(id:string){
    this.spotify.getTopTracks(id).subscribe(tracks =>{
      this.topTracks = tracks;
      console.log(this.topTracks);
      
    });

  }

  ngOnInit(): void {


  }

}
