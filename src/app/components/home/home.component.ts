import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  allRecomended:any;
  searching:boolean=false;

  constructor(private spotifyService: SpotifyService) { 
    
  }

  ngOnInit(): void {
    this.cargarContenido();

  }
  cargarContenido(){
    this.searching =true;
    this.spotifyService.getAllRecomended().subscribe((songs:any)=>{
      console.log(songs);
      
      this.allRecomended = songs;
      this.searching = false;
    });
  }

}
