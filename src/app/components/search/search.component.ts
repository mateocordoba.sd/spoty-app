import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  artists:any[]=[];
  searching:boolean = false;

  constructor(private spotyfyService:SpotifyService) { }

  ngOnInit(): void {
  }
  buscar(termino:string){
    this.searching = true;
    this.spotyfyService.searchArtist(termino).subscribe((data:any)=>{
      this.artists = data;
      this.searching = false;
    })
    
  }
}
