import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noImage'
})
export class NoImagePipe implements PipeTransform {

  transform(images: any[]): string {
    return !images ? 'assets/img/noimage.png' : images.length > 0 ? images[0].url :'assets/img/noimage.png'
  }


}
